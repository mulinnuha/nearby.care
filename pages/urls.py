from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^about/$', views.about, name='about'),

    url(r'^institution/(?P<query>\w+([ %\d+]+\w+)*)/$', views.institution, name='institution'),
    url(r'^institution/(?P<query>\w+([ %\d+]+\w+)*)/(?P<latitude>\d+\.\d+)/(?P<longitude>\d+\.\d+)/$', views.institution, name='institution'),

    url(r'^caregiver/(?P<query>\w+([ %\d+]+\w+)*)/$', views.caregiver, name='caregiver'),
    url(r'^caregiver/(?P<query>\w+([ %\d+]+\w+)*)/(?P<latitude>\d+\.\d+)/(?P<longitude>\d+\.\d+)/$', views.caregiver, name='caregiver'),

    url(r'^trial/(?P<query>\w+([ %\d+]+\w+)*)/$', views.trial, name='trial'),
    url(r'^trial/(?P<query>\w+([ %\d+]+\w+)*)/(?P<latitude>\d+\.\d+)/(?P<longitude>\d+\.\d+)/$', views.trial, name='trial'),

    url(r'^details_institution/(?P<institution_id>\d+)$', views.details_institution, name='details_institution'),
    url(r'^details_caregiver/(?P<caregiver_id>\d+)$', views.details_caregiver, name='details_caregiver'),
    url(r'^details_trial/(?P<trial_id>\d+)$', views.details_trial, name='details_trial')

]

