from django.shortcuts import render
from django.http import Http404
from django.http import JsonResponse
from django.core import serializers

from medical import querying, serializers, models


# Views
def index(request):
    return render(request, 'pages/index.html')


def about(request):
    return render(request, 'pages/about.html')


def details_institution(request, institution_id):
    info = querying.query_institution_info(institution_id)
    serializer_info = serializers.MedicalInstitutionListSerializer(info, many=True)


    emp = querying.query_institution_employee(institution_id)
    serializer_emp = serializers.MedicalEmploymentListSerializer(emp, many=True)

    trials = querying.query_institution_trials(institution_id)
    serializer_trials = serializers.MedicalCaregiverTrialListSerializer(trials, many=True)

    context = {'info': serializer_info.data, 'employment': serializer_emp.data, 'trials': serializer_info.data}
    return render(request, 'pages/details_institution.html', context)
    #return JsonResponse(serializer_trials.data, safe=False)


def details_caregiver(request, caregiver_id):
    info = querying.query_caregiver_info(caregiver_id)
    serializer_info = serializers.MedicalCaregiverListSerializer(info, many=True)

    publication = querying.query_caregiver_publication(caregiver_id)
    serializer_pub = serializers.MedicalPublicationListSerializer(publication, many=True)

    trials = querying.query_caregiver_trials(caregiver_id)
    serializer_trials = serializers.MedicalCaregiverTrialListSerializer(trials, many=True)

    context = {'info': serializer_info.data, 'publication': serializer_pub.data, 'trials': serializer_trials.data}
    return render(request, 'pages/details_caregiver.html', context)
    #return JsonResponse(serializer_pub.data, safe=False)


def details_trial(request, trial_id):
    info = querying.query_trial_info(trial_id)
    serializer_info = serializers.MedicalTrialListSerializer(info, many=True)

    context = {'info': serializer_info.data}
    return render(request, 'pages/details_trial.html', context)
    #return JsonResponse(serializer_info.data, safe=False)


def institution(request, query, latitude=None, longitude=None):
    if latitude and longitude:
        try:
            latitude = float(latitude)
            longitude = float(longitude)
        except ValueError:
            raise Http404

    query = query.replace('%20', ' ')
    data = querying.query_institutions(query, latitude, longitude)
    serializer = serializers.MedicalInstitutionListSerializer(data, many=True)
    return JsonResponse(serializer.data, safe=False)


def caregiver(request, query, latitude=None, longitude=None):
    if latitude and longitude:
        try:
            latitude = float(latitude)
            longitude = float(longitude)
        except ValueError:
            raise Http404

    query = query.replace('%20', ' ')
    data = querying.query_caregivers(query, latitude, longitude)
    serializer = serializers.MedicalCaregiverListSerializer(data, many=True)
    return JsonResponse(serializer.data, safe=False)


def trial(request, query, latitude=None, longitude=None):
    if latitude and longitude:
        try:
            latitude = float(latitude)
            longitude = float(longitude)
        except ValueError:
            raise Http404

    query = query.replace('%20', ' ')
    data = querying.query_trials(query, latitude, longitude)
    serializer = serializers.MedicalTrialListSerializer(data, many=True)
    return JsonResponse(serializer.data, safe=False)
