django>=1.10,<1.11
django-countries==4.1
djangorestframework
json-lines==0.2.0
scrapy
requests
xmltodict
fuzzywuzzy[speedup]
mtranslate
numpy
biopython
geocoder
geotext
nltk

