# nearby.care

## Demo

[http://nearbycare.ulinnuha.id/](http://nearbycare.ulinnuha.id/)

## Installation

```bash
pip install virtualenv
virtualenv -p python3.5 nearby.care
cd nearby.care
git clone https://github.com/jorenham/nearby.care.git
source bin/activate
cd nearby.care
pip install -r requirements_dev.txt
```

## Run dev server
```bash
python manage.py runserver
```

## zorgkaartnederland 
Scrape
```bash
python scraping/zorgkaartnederland.py
```
Import
```bash
python manage.py import_zorgkaartnederland
```

## clinicaltrials
Import
```bash
python manage.py import_clinicaltrials
```

## pubmed
Import
```bash
python manage.py import_pubmed
```

## Testing
```bash
pip install tox
tox
```

## Contributing
- Don't push directly to master! Heroku deploys from here.
- Use pull-requests and review them.
- Changed a model? Run `python manage.py makemigrations` first.
