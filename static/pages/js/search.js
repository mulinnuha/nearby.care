var CATEGORY_INSTITUTION = 1;
var CATEGORY_CAREGIVER = 2;
var CATEGORY_TRIAL = 3;

$("#submit-button").click(function () {
    var searchCategory = $("#search-category").val();
	if(searchCategory == CATEGORY_INSTITUTION){
	    getResult();
	} else if(searchCategory == CATEGORY_CAREGIVER){
	    getCaregiver();
	} else if(searchCategory == CATEGORY_TRIAL){
	    getTrial();
	} else {
	    console.log("Invalid search category");
	}
});

$("#choose-hospital").click(function () {
	$("#btn-dropdown").html('Medical institution <span class="caret"></span>');
	$("#search-category").val(CATEGORY_INSTITUTION);
	$("#submit-button").click();
});

$("#choose-doctor").click(function () {
	$("#btn-dropdown").html('Medical caregiver <span class="caret"></span>');
	$("#search-category").val(CATEGORY_CAREGIVER);
	$("#submit-button").click();
});

$("#choose-trial").click(function () {
	$("#btn-dropdown").html('Medical trial <span class="caret"></span>');
	$("#search-category").val(CATEGORY_TRIAL);
	$("#submit-button").click();
});


$("#search-input").keyup(function (event) {
	if (event.keyCode === 13) {
		$("#submit-button").click();
	}
});

var mapLeaflet = null;

if (window.location.hash.substr(1)) {
    $("#search-input").val(window.location.hash.substr(1))
    getResult();
}

function getResult() {
    if (mapLeaflet != null) {
        mapLeaflet.off();
        mapLeaflet.remove();
    }

	var q = $("#search-input").val();

    if (q === '') {
        return;
    }

    window.location.hash = q;

    var url = "institution/" + q + "/";
    if (locationIsRetrieved) {
        url += coords.latitude + "/" + coords.longitude + "/"
    }

    var geojson = [];

    $.getJSON(url, function (data) {
        $("#result-list").html("");

        $.each(data, function (i, item) {
            var number = i + 1;
            var item_name = '<h3 class="inst-name">' + number + '. ' + '<a href="/details_institution/' + item.id + '" target="_blank">' + item.name + '</a></h3>';
            var item_inst_type = '<span class="inst-type">' + "Type: " + item.inst_type + '</span></br>';
            var item_address = '<span class="inst-addr">' + item.address + '</span></br>';
            var item_city = '<span class="inst-addr">' + item.postal_code + ' ' + item.city + ' ' + item.country + '</span></br>';

            var item_url = "";
            if (item.url) {
                item_url = '<span class="website">' + '<a href="http://' + item.url + '" target="_blank">' + item.url + '</a>';
            }

            // feedback for evaluation
            var item_feedback = '' +
                '<a href="#" title="Like">' +
                    '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>' +
                '</a>' +
                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                '<a href="#" title="Dislike">' +
                    '<i class="fa fa-thumbs-o-down" aria-hidden="true"></i>' +
                '</a>' +
                '<br/>';

            var item_rating = "";
            if (item.rating) {
                item_rating = '<h1 class="rating">' + item.rating + '</h1>';
            } else {
                item_rating = '<h1 class="rating"> - </h1>';
            }

            var item_distance = "";
            if (item.distance && item.distance !== -1) {
                var distance_str;
                if (item.distance < 1) {
                    distance_str = (item.distance * 1000).toFixed(0) + "m"
                } else {
                    distance_str = item.distance.toFixed(1) + "km"
                }
                item_distance = '<span class="inst-distance">' + distance_str + '</span>'
            }


            $('#result-list').append(
                '<div class="list-group-item">' +
                    '<div class="row">' +
                        '<div class="col-lg-10">' +
                            item_name + item_inst_type + item_address + item_city + item_url +
                        '</div>' +
                        '<div class="col-lg-2">' +
                            item_feedback + item_rating + item_distance +
                        '</div>' +
                    '</div>' +
                '</div>'
            );

            geojson.push({
                'type': 'Feature',
                'geometry': {
                    'type': 'Point',
                    'coordinates': [item.longitude, item.latitude]
                },
                properties: {
                    'title': item.name,
                    'description': item.inst_type,
                    'marker-color': '#63b6e5',
                    'marker-size': 'large',
                    'marker-symbol': number.toString()
                }
            });
        });

        $('#result-list').append(
            '<button type="button" class="btn btn-warning" style="margin: 1em 0">' +
                'I did not find what I was looking for' +
            '</button>'
        );

        L.mapbox.accessToken =
            'pk.eyJ1IjoibXVsaW5udWhhIiwiYSI6ImNqMHduc3N2MDAwMDEzNHBneG81Nmhkc2gifQ.Zk8qXwxZPjvJBDqUpBrveQ';

        mapLeaflet = L.mapbox.map('map-placeholder', 'mapbox.light')
            .setView([52.001667, 4.3725], 10);

        var myLayer = L.mapbox.featureLayer().setGeoJSON(geojson).addTo(mapLeaflet);

        mapLeaflet.scrollWheelZoom.disable();
    });
}

function getCaregiver() {
    if (mapLeaflet != null) {
        mapLeaflet.off();
        mapLeaflet.remove();
    }

	var q = $("#search-input").val();

    if (q === '') {
        return;
    }

    window.location.hash = q;

    var url = "caregiver/" + q + "/";
    if (locationIsRetrieved) {
        url += coords.latitude + "/" + coords.longitude + "/"
    }

    var geojson = [];

    $.getJSON(url, function (data) {
        $("#result-list").html("");

        $.each(data, function (i, item) {
            var number = i + 1;
            var item_name = '<h3 class="caregiver-name">' + number + '. ' + '<a href="/details_caregiver/' + item.id + '" target="_blank">' + item.name + '</a></h3>';
            var item_profession = '<span class="caregiver-prof">' + "Profession: " + item.profession + '</span></br>';
            var separator = '<h5>Works in: </h5>';
            var item_inst = '';

            // feedback for evaluation
            var item_feedback = '' +
                '<a href="#" title="Like">' +
                    '<i class="fa fa-thumbs-o-up" aria-hidden="true"></i>' +
                '</a>' +
                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                '<a href="#" title="Dislike">' +
                    '<i class="fa fa-thumbs-o-down" aria-hidden="true"></i>' +
                '</a>' +
                '<br/>';

            $.each(item.medical_institutions, function (i, element) {

                var el_name = '<h4 class="inst-name">' + '<a href="/details_institution/' + element.id + '" target="_blank">' + element.name + '</a></h4>';
                var el_inst_type = '<span class="inst-type">' + 'Type: ' + element.inst_type + '</span></br>';
                var el_address = '<span class="inst-addr">' + element.address + '</span></br>';
                var el_city = '<span class="inst-addr">' + element.postal_code + ' ' + element.city + ' ' + element.country + '</span></br>';

                var el_url = "";
                if (element.url) {
                    el_url = '<span class="website">' + '<a href="http://' + element.url + '" target="_blank">' + element.url + '</a>';
                }

                var item_rating = "";
                if (element.rating) {
                    item_rating = '<h1 class="rating">' + element.rating + '</h1>';
                } else {
                    item_rating = '<h1 class="rating"> - </h1>';

                }

                var item_distance = "";
                if (element.distance && element.distance !== -1) {
                    var distance_str;
                    if (element.distance < 1) {
                        distance_str = (element.distance * 1000).toFixed(0) + "m"
                    } else {
                        distance_str = element.distance.toFixed(1) + "km"
                    }

                     item_distance = '<span class="inst-distance">' + distance_str + '</span>'

                }

                item_inst += '' +
                    '<div class="row">' +
                        '<div class="col-lg-10">' +
                            el_name + el_inst_type + el_address + el_city + el_url + "<br/>" +
                        '</div>' +
                        '<div class="col-lg-2">' +
                            item_rating + item_distance +
                        '</div>' +
                    '</div>' +
                    '<div class="row">&nbsp;</div>';

                geojson.push({
                    'type': 'Feature',
                    'geometry': {
                        'type': 'Point',
                        'coordinates': [element.longitude, element.latitude]
                    },
                    properties: {
                        'title': element.name,
                        'description': element.inst_type,
                        'marker-color': '#63b6e5',
                        'marker-size': 'large',
                        'marker-symbol': number.toString()
                    }
                });
            });

            $('#result-list').append(
                '<div class="list-group-item">' +
                    '<div class="row">' +
                        '<div class="col-lg-12">' +
                            '<div class="row">' +
                                '<div class="col-lg-10">' +
                                    item_name + item_profession + separator +
                                '</div>' +
                                '<div class="col-lg-2">' +
                                    item_feedback +
                                '</div>' +
                            '</div>' +
                            item_inst +
                        '</div>' +
                    '</div>' +
                 '</div>'
            );

        });

        $('#result-list').append(
            '<button type="button" class="btn btn-warning" style="margin: 1em 0">' +
            'I did not find what I was looking for' +
            '</button>'
        );

        L.mapbox.accessToken =
            'pk.eyJ1IjoibXVsaW5udWhhIiwiYSI6ImNqMHduc3N2MDAwMDEzNHBneG81Nmhkc2gifQ.Zk8qXwxZPjvJBDqUpBrveQ';

        mapLeaflet = L.mapbox.map('map-placeholder', 'mapbox.light')
            .setView([52.001667, 4.3725], 10);

        var myLayer = L.mapbox.featureLayer().setGeoJSON(geojson).addTo(mapLeaflet);

        mapLeaflet.scrollWheelZoom.disable();
    });
}

function getTrial() {
    /*
	if (mapLeaflet != null) {
        mapLeaflet.off();
        mapLeaflet.remove();
    }
    */

	var q = $("#search-input").val();

    if (q === '') {
        return;
    }

    window.location.hash = q;

    var url = "trial/" + q + "/";
    if (locationIsRetrieved) {
        url += coords.latitude + "/" + coords.longitude + "/"
    }

    var geojson = [];

    $.getJSON(url, function (data) {
        $("#result-list").html("");

        $.each(data, function (i, item) {
            var number = i + 1;
            var item_brief_title ='<h3><a href="/details_trial/' + item.id + '" target="_blank">'+item.brief_title+'</a></h3>';
            var item_type = item.type;
            var item_status = item.status;

            var item_date = item.start_date + ' - ' + item.completion_date ;
            if(item.completion_date == null){
                var item_date = item.start_date + ' - now';
            }

            var item_investigator = '';
            if (item.investigator) {
                item_investigator = 'investigator: ';
                if (item.investigator_caregiver) {
                    item_investigator += '<a href="/details_caregiver/'+item.investigator_caregiver+'" target="_blank">'+item.investigator+'</a>';
                } else {
                    item_investigator += '<span>' + item.investigator + '</span>';
                }
            }

            var item_institution = '';
            if (item.investigator_institution) {
                if (item.investigator_institution_relation) {
                    item_institution += '<a href="/details_institution/'+item.investigator_institution_relation+'" target="_blank">'+item.investigator_institution+'</a>';
                } else {
                    item_institution += '<span>' + item.investigator_institution + '</span>';
                }
            }

            var item_distance = "";
            if (item.distance) {
                var distance_str;
                if (item.distance < 1) {
                    distance_str = (item.distance * 1000).toFixed(0) + "m"
                } else {
                    distance_str = item.distance.toFixed(1) + "km"
                }

                item_distance = '<h2 class="inst-distance">' + distance_str + '</h2>'

            }

            $('#result-list').append(
                '<div class="list-group-item">' +
                    '<div class="row">' +
                        '<div class="col-lg-12">' +
                            item_brief_title +
                        '</div>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-lg-8">' +
                            "type: " + item_type + "</br>" + "status: " + item_status + "</br>" + item_date + "</br>" +
                            item_investigator +
                        '</div>' +
                        '<div class="col-lg-4">' +
                            item_distance + item_institution +
                        '</div>' +
                    '</div>' +
                '</div>'
            );

            if (item.distance) {
                geojson.unshift({
                    'type': 'Feature',
                    'geometry': {
                        'type': 'Point',
                        'coordinates': [item.longitude, item.latitude]
                    },
                    properties: {
                        'title': item_brief_title,
                        'description': item_brief_title,
                        'marker-color': '#63b6e5',
                        'marker-size': 'large',
                        'marker-symbol': number.toString()
                    }
                });
            }

        });

        $('#result-list').append(
            '<button type="button" class="btn btn-warning" style="margin: 1em 0">' +
            'I did not find what I was looking for' +
            '</button>'
        );

        if (geojson.length > 0) {
            if (!mapLeaflet) {
                L.mapbox.accessToken =
                    'pk.eyJ1IjoibXVsaW5udWhhIiwiYSI6ImNqMHduc3N2MDAwMDEzNHBneG81Nmhkc2gifQ.Zk8qXwxZPjvJBDqUpBrveQ';

                mapLeaflet = L.mapbox.map('map-placeholder', 'mapbox.light')
                    .setView([52.001667, 4.3725], 10);

                mapLeaflet.scrollWheelZoom.disable();
            }

            L.mapbox.featureLayer().setGeoJSON(geojson).addTo(mapLeaflet);
        }
    });
}

var $loading = $('#modal').hide();
$(document).ajaxStart(function () {
    $loading.show();
}).ajaxStop(function () {
    $loading.hide();
});
