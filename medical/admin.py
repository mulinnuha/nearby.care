from django.contrib import admin
from . import models

# Utility base classes


class ReadOnlyTabularInline(admin.TabularInline):
    extra = 0
    can_delete = False
    editable_fields = []
    readonly_fields = []
    exclude = []

    def get_readonly_fields(self, request, obj=None):
        return list(self.readonly_fields) + \
               [field.name for field in self.model._meta.fields
                if field.name not in self.editable_fields and
                field.name not in self.exclude]

    def has_add_permission(self, request):
        return False


class ReadOnlyModelAdmin(admin.ModelAdmin):
    readonly_fields = []

    def get_readonly_fields(self, request, obj=None):
        return list(self.readonly_fields) + \
               [field.name for field in obj._meta.fields] + \
               [field.name for field in obj._meta.many_to_many]

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


# Inlines

class EmploymentInline(ReadOnlyTabularInline):
    model = models.Employment


class TrialInline(ReadOnlyTabularInline):
    model = models.Trial
    fields = ('brief_title', 'status', 'start_date', 'completion_date')


# Model admins


@admin.register(models.MedicalInstitutionType)
class MedicalInstitutionTypeAdmin(ReadOnlyModelAdmin):
    pass


@admin.register(models.MedicalInstitution)
class MedicalInstitutionAdmin(ReadOnlyModelAdmin):
    list_display = ('name_clean', 'type', 'location', 'url', 'rating', 'rating_count')
    search_fields = ('name', 'type__name', 'city', 'address', 'postal_code', 'country')
    fieldsets = (
        (None, {
            'fields': ('id', 'type', 'name')
        }),
        ('Contact', {
            'fields': ('address', 'city', 'country', 'postal_code', 'telephone', 'url')
        }),
        ('Geo', {
            'fields': ('latitude', 'longitude')
        }),
        ('Rating', {
            'fields': ('rating', 'rating_count')
        })
    )


@admin.register(models.Specialism)
class SpecialismAdmin(ReadOnlyModelAdmin):
    pass


@admin.register(models.Profession)
class ProfessionAdmin(ReadOnlyModelAdmin):
    pass


@admin.register(models.Caregiver)
class CaregiverAdmin(ReadOnlyModelAdmin):
    list_display = ('name', 'gender', 'profession', 'institution', 'rating', 'rating_count')
    inlines = [EmploymentInline, TrialInline]
    search_fields = ('name', 'medical_institutions__name', 'employments__profession__name', 'employments__specialism__name')


@admin.register(models.Trial)
class TrialAdmin(ReadOnlyModelAdmin):
    readonly_fields = ('conditions_flat', 'treatments_flat', 'investigator_caregiver_certainty')
    fieldsets = (
        (None, {
            'fields': ('acronym', 'brief_title', 'title', 'summary', 'description', 'type')
        }),
        ('Sponsors', {
            'fields': ('lead_sponsor', 'collaborators')
        }),
        ('Status', {
            'fields': ('status', 'start_date', 'completion_date', 'primary_completion_date', 'phase')
        }),
        ('Criteria', {
            'fields': ('gender', 'min_age', 'max_age', 'healthy_volunteers', 'criteria')
        }),
        ('Investigator', {
            'fields': ('investigator', 'investigator_title', 'investigator_institution',
                       ('investigator_caregiver', 'investigator_caregiver_certainty'),
                       ('investigator_institution_relation', 'investigator_institution_relation_certainty'))
        }),
        ('Disease', {
            'fields': ('conditions_flat', 'treatments_flat')
        })
    )
    list_display = ('brief_title', 'investigator_caregiver', 'investigator_institution_relation')

    def conditions_flat(self, trial):
        return ', '.join(trial.conditions.all().values_list('term', flat=True))
    conditions_flat.short_description = 'Conditions (MeSH)'

    def treatments_flat(self, trial):
        return ', '.join(trial.treatments.all().values_list('name', flat=True))
    treatments_flat.short_description = 'Treatments'


@admin.register(models.Author)
class AuthorAdmin(ReadOnlyModelAdmin):
    pass


@admin.register(models.Publication)
class PublicationAdmin(ReadOnlyModelAdmin):
    list_display = ('title', 'date', 'get_authors')

    def get_authors(self, publication):
        return ', '.join(author.name for author in publication.authors.all())
    get_authors.short_description = 'authors'
