from statistics import mean
from geotext import GeoText

import geocoder
import os
import nltk

from django.db import connection
from django.db.models import Avg, Q, Prefetch
from django.conf import settings
from . import models

from mtranslate import translate

def stopwords_removal(query):
    NLTK_DIR = os.path.join(settings.BASE_DIR, 'nltk')
    nltk.data.path.append(NLTK_DIR)

    disallowed_tags = ['IN', 'PRP']
    new_tokens = []

    tokens = nltk.word_tokenize(query)
    tagged = nltk.pos_tag(tokens)
    for word in tagged:
        if word[1] not in disallowed_tags:
            new_tokens.append(word[0])

    new_query = ' '.join(new_tokens)
    return new_query


def recognize_city(word):
    geo_parser = GeoText(word)
    city = geo_parser.cities

    return city


def recognize_location(query):
    new_query = ''
    for keyword in query.split(' '):
        if recognize_city(keyword.title()):
            city = keyword
            new_query = query.replace(city, '').lstrip().rstrip()

    if new_query:
        return new_query, city
    else:
        return query, None


def get_latitude_longitude(location):
    geocode = geocoder.google(location)
    latitude = geocode.latlng[0]
    longitude = geocode.latlng[1]

    return latitude, longitude


def natural_language_processing(query, latitude=None, longitude=None):
    query = stopwords_removal(query)
    query, city = recognize_location(query)
    if city:
        latitude, longitude = get_latitude_longitude(city)

    translated_query = translate(query, "nl", "en")

    return translated_query, latitude, longitude


def query_institutions(query, latitude=None, longitude=None):
    max_results = 20

    specialism_reward = 30

    query, latitude, longitude = natural_language_processing(query, latitude, longitude)

    query_filter = Q(name__icontains=query) | Q(type__name__icontains=query) | Q(specialisms__description__icontains=query)
    query_result = models.MedicalInstitution.objects.filter(query_filter)

    if latitude and longitude:
        proximity = 100
        result = models.MedicalInstitution.objects.nearby(latitude, longitude, proximity).filter(query_filter)
    else:
        result = query_result

    min_rating_count = 5
    avg_rating = models.MedicalInstitution.objects\
        .filter(rating__isnull=False, rating_count__gt=min_rating_count)\
        .aggregate(avg_rating=Avg('rating'))['avg_rating']

    def rank(institution):
        rating_deviation = 0
        if institution.rating and institution.rating_count > min_rating_count:
            rating_deviation = float(institution.rating) - avg_rating

        # either all have distance or none
        sort_rank = institution.distance if hasattr(institution, 'distance') else 0
        sort_rank -= rating_deviation**3  # third power retains the sign

        relevant_specialisms = institution.specialisms.filter(description__icontains=query)

        return sort_rank

    return sorted(result, key=rank)[:max_results]


def query_trials(query, latitude=None, longitude=None):
    max_results = 20
    query_filter = Q(treatments__name__icontains=query) |\
                   Q(conditions__term__icontains=query) |\
                   Q(summary__icontains=query)

    institutions = models.MedicalInstitution.objects.filter(trials__isnull=False)
                              
    query_result = models.Trial.objects.filter(query_filter)
    if connection.vendor == 'postgresql':
        query_result = query_result.distinct()

    def rank(trial):
        nearby_reward_factor = 0.3
        default_distance = 100
        investigator_reward = 1
        caregiver_reward_factor = 2
        institution_certainty_threshold = 0.71

        sort_rank = 0

        status_rank_map = {
            ("AM",): 1,
            ("CM",): 2,
            ("RC",): 3,
            ("NR",): 4,
            ("TN", "WH", "SP", "WD"): 100
        }
        for status, rank_ in status_rank_map.items():
            if trial.status in status:
                sort_rank = rank_
                break

        if trial.investigator:
            sort_rank -= investigator_reward
            sort_rank -= trial.investigator_caregiver_certainty * caregiver_reward_factor

        if trial.investigator_institution and latitude and longitude:
            if trial.investigator_institution_relation_certainty > institution_certainty_threshold:
                trial.distance = trial.investigator_institution_relation.manual_distance(latitude, longitude)
                trial.latitude = trial.investigator_institution_relation.latitude
                trial.longitude = trial.investigator_institution_relation.longitude
                sort_rank -= default_distance - trial.distance * nearby_reward_factor
        
        return sort_rank    
        
    return sorted(query_result, key=rank)[:max_results]


def query_caregivers(query, latitude=None, longitude=None):
    max_results = 20

    query, latitude, longitude = natural_language_processing(query, latitude, longitude)

    institution_ids = [i.id for i in query_institutions(query, latitude, longitude)]

    query_filter = (Q(name__icontains=query) | Q(employments__profession__name__icontains=query)) \
                   & Q(medical_institutions__in=institution_ids)

    # force to calculate the distance
    query_result = models.Caregiver.objects.filter(query_filter).prefetch_related(
        Prefetch('medical_institutions', queryset=models.MedicalInstitution.objects.nearby(latitude, longitude))
    )

    min_rating_count = 5
    avg_rating = mean(caregiver.rating for caregiver in models.Caregiver.objects.all() if caregiver.rating and caregiver.rating > min_rating_count)

    def rank(caregiver):
        no_rating_penalty = 2
        rating_weight = 2
        publication_count_weight = 0.1

        rating_deviation = 0
        if caregiver.rating and caregiver.rating_count > min_rating_count:
            rating_deviation = float(caregiver.rating) - float(avg_rating)
            rating_deviation *= rating_weight
            print(caregiver.name, rating_deviation)
        else:
            rating_deviation -= no_rating_penalty

        # either all have distance or none
        if latitude and longitude:
            sort_rank = caregiver.distance_avg(latitude, longitude)
        else:
            sort_rank = 0

        sort_rank -= rating_deviation

        sort_rank -= caregiver.publication_count * publication_count_weight

        return sort_rank

    return sorted(query_result[:max_results], key=rank)


def query_institution_info(institution_id):
    query_filter = Q(id__exact=institution_id)
    query_result = models.MedicalInstitution.objects.filter(query_filter).prefetch_related('type')
    return query_result


def query_institution_employee(institution_id):
    query_filter = Q(medical_institution_id__exact=institution_id)
    query_result = models.Employment.objects.filter(query_filter).prefetch_related('caregiver')
    return query_result


def query_institution_trials(institution_id):
    query_filter = Q(investigator_institution_relation_id__exact=institution_id)
    query_result = models.Trial.objects.filter(query_filter)
    return query_result


def query_caregiver_info(caregiver_id):
    query_filter = Q(id__exact=caregiver_id)
    query_result = models.Caregiver.objects.filter(query_filter)
    return query_result


def query_caregiver_employment(caregiver_id):
    query_filter = Q(id__exact=caregiver_id)
    query_result = models.Caregiver.objects.filter(query_filter).prefetch_related('medical_institutions')
    return query_result


def query_caregiver_publication(caregiver_id):
    query_filter = Q(authors__author_caregiver_id__exact=caregiver_id)
    query_result = models.Publication.objects.filter(query_filter)
    return query_result


def query_caregiver_trials(caregiver_id):
    query_filter = Q(investigator_caregiver_id__exact=caregiver_id)
    query_result = models.Trial.objects.filter(query_filter)
    return query_result


def query_trial_info(trial_id):
    query_filter = Q(id__exact=trial_id)
    query_result = models.Trial.objects.filter(query_filter)
    return query_result
