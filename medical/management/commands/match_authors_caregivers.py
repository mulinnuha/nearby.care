import logging

from django.core.management.base import BaseCommand
from django.db import transaction

from medical import models

logger = logging.getLogger('main')


class Command(BaseCommand):
    help = 'Match authors with caregivers'

    @transaction.atomic
    def handle(self, *args, **options):
        total_count, match_count, certainty_sum = 0, 0, 0
        for author in models.Author.objects.all():
            total_count += 1
            match, certainty = author.match_author_caregiver()
            if match:
                match_count += 1
                certainty_sum += certainty
                self.stdout.write('{:>30}: {:>30} match found ({})'.format(author.name, match.name, certainty))

        self.stdout.write('\ntotal: {}/{} matches with an average certainty of {}'.format(match_count, total_count, certainty_sum/match_count))
