import os
import logging
from datetime import timedelta
from functools import wraps

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

import json_lines
from django.db import transaction

from medical import models

logger = logging.getLogger('main')


def _log_progess(message, out=None):
    def wrap(func):
        @wraps(func)
        def wrapped_func(*args, **kwargs):
            out_ = out if out else args[0].stdout
            out_.write('{} ....'.format(message))
            res = func(*args, **kwargs)
            out_.write('{} [OK]'.format(message))
            return res
        return wrapped_func
    return wrap


class Command(BaseCommand):
    FILE_PATH = os.path.join(settings.BASE_DIR, 'scraping/zorgkaartnederland.jl')

    help = 'Imports scraped .jl file from zorgkaartnederland'

    def handle(self, *args, **options):
        if not os.path.isfile(self.FILE_PATH):
            raise CommandError('No scraped data available')

        #self._flush()

        self._import_jl('MedicalInstitution', self._import_institution)
        self._import_jl('Specialism', self._import_specialism)
        self._import_jl('Caregiver', self._import_caregiver)
        self._import_jl('Employment', self._import_employment)

        models.Caregiver.objects.fill_rating()

    @_log_progess('Flushing data')
    def _flush(self):
        model_names = (
            'Employment', 'Caregiver', 'Specialism', 'Profession', 'MedicalInstitution', 'MedicalInstitutionType'
        )
        for model_name in model_names:
            getattr(models, model_name).objects.all().delete()

    def _import_jl(self, item_type, import_func):
        @_log_progess('Importing {} items'.format(item_type), out=self.stdout)
        @transaction.atomic
        def __import_jl():
            count = 0
            with open(self.FILE_PATH, 'rb') as file:
                for item in json_lines.reader(file):
                    if item['item_type'] == item_type:
                        import_func(item)
                        count += 1

        return __import_jl()

    def _import_institution(self, item):
        type_name = item.get('category', 'N/A')
        institution_type, _ = models.MedicalInstitutionType.objects.get_or_create(name=type_name)
        institution, _ = models.MedicalInstitution.objects.update_or_create(
            id=item['id'],
            defaults={
                'type': institution_type,
                'name': item['title'],
                'address': item.get('address', ''),
                'city': item.get('city', ''),
                'country': 'NL',
                'postal_code': item.get('postal_code'),
                'telephone': item.get('telephone'),
                'url': item.get('url'),
                'rating': item.get('rating'),
                'rating_count': item.get('rate_count', 0),
                'latitude': item['latitude'],
                'longitude': item['longitude']
            }
        )
        return institution

    def _import_specialism(self, item):
        institution_id = item['medical_institution']
        institution = models.MedicalInstitution.objects.get(id=institution_id)
        waiting_period = timedelta(weeks=item['waiting_period']) if 'waiting_period' in item else None

        specialism, _ = models.Specialism.objects.update_or_create(
            name=item['name'],
            defaults={
                'medical_institution': institution,
                'name': item['name'],
                'description': item.get('description',''),
                'waiting_period': waiting_period
            }
        )
        return specialism

    def _import_caregiver(self, item):
        caregiver, _ = models.Caregiver.objects.update_or_create(
            id=item['id'],
            defaults={
                'name': item['name'],
                'gender': item['gender'] == 'Man' if 'gender' in item else None,
            }
        )
        return caregiver

    def _import_employment(self, item):
        caregivers = models.Caregiver.objects.filter(id=item['caregiver'])
        error_msg = 'Failed to import Employment: '
        if not caregivers:
            self.stderr.write('{}unknown Caregiver id {}'.format(error_msg, item['caregiver']))
            return None

        institutions = models.MedicalInstitution.objects.filter(id=item['medical_institution'])
        if not institutions:
            self.stderr.write('{}unknown MedicalInstitution id {}'.format(error_msg, item['medical_institution']))
            return None

        profession = models.Profession.objects.get_or_create(
            name = (item['profession'])
        )[0] if 'profession' in item else None
        employment, _ = models.Employment.objects.update_or_create(
            medical_institution=institutions.first(),
            caregiver=caregivers.first(),
            defaults={
                'profession': profession,
                'rating': item.get('rating'),
                'rating_count': item.get('rate_count', 0)
            }
        )
        return employment
