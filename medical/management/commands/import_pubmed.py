import datetime
import os
import logging
import re

from django.core.management.base import BaseCommand
from django.conf import settings
from django.db import transaction

from medical import models
from scraping import pubmed

logger = logging.getLogger('main')


class Command(BaseCommand):

    help = 'Imports downloaded .zip file from Entrez'

    def handle(self, *args, **options):
        log_msg = 'Importing Pubmed items'
        self.stdout.write('%s....' % log_msg)

        batch_size = 1e3

        count = 0
        with transaction.atomic():
            for pubmed_file in pubmed.medical_publications():
                pubmed_obj = self._parse_pubmed(pubmed_file)
                if pubmed_obj:
                    count += 1
                    if not count % batch_size:
                        self.stdout.write('{} [{}k]'.format(log_msg, count / 1e3))

        self.stdout.write('%s [OK]' % log_msg)


    def _parse_pubmed(self, pubmed_file):
        pubmed_obj, _ = models.Publication.objects.update_or_create(
                pmid = pubmed_file['pmid'],
                defaults = {
                    'title': pubmed_file['title'],
                    'date': datetime.datetime.strptime(pubmed_file['date'], "%Y%m%d").date(),
                }
        )
        self.__validate_object(pubmed_obj)

        for t in self._mesh_terms(pubmed_file):
            mesh, _ = models.Mesh.objects.update_or_create(term = t)
            pubmed_obj.mesh_term.add(mesh)

        for a in self._authors(pubmed_file):
            author, _ = models.Author.objects.update_or_create(name = a)
            pubmed_obj.authors.add(author)

    def _mesh_terms(self, pubmed_file):
        condition_browse = pubmed_file.get('mesh_term')
        if not condition_browse:
            return []

        mesh_term = condition_browse
        if isinstance(mesh_term, str):
            return [mesh_term]
        else:
            return mesh_term

    def _authors(self, pubmed_file):
        condition_browse = pubmed_file.get('author_full')
        if not condition_browse:
            return []

        author = condition_browse
        if isinstance(author, str):
            return [author]
        else:
            return author

    def __validate_object(self, obj):
        if settings.DEBUG:
            obj.full_clean()