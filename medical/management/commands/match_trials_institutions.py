import logging

from django.core.management.base import BaseCommand
from django.db import transaction

from medical import models

logger = logging.getLogger('main')


class Command(BaseCommand):
    help = 'Match medical trails with medical institutions'

    @transaction.atomic
    def handle(self, *args, **options):
        total_count, match_count, certainty_sum = 0, 0, 0
        for trial in models.Trial.objects.all():
            total_count += 1
            institutution, certainty = trial.match_investigator_institution(save=True)
            if institutution:
                match_count += 1
                certainty_sum += certainty
                self.stdout.write('{}\n{}\n->{}%\n'.format(trial.investigator_institution, institutution, certainty))

        self.stdout.write('\ntotal: {}/{} matches with an average certainty of {}'.format(match_count, total_count, certainty_sum/match_count))


