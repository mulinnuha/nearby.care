import datetime
import os
import logging
import re

from django.core.management.base import BaseCommand
from django.conf import settings
from django.db import transaction

from medical import models
from scraping import clinicaltrials

logger = logging.getLogger('main')


class Command(BaseCommand):
    FILE_PATH = os.path.join(settings.BASE_DIR, 'scraping/zorgkaartnederland.jl')

    help = 'Imports downloaded .zip file from clinicaltrials.gov'

    def handle(self, *args, **options):
        log_msg = 'Importing Trial items'
        self.stdout.write('%s....' % log_msg)

        batch_size = 1e3

        count = 0
        with transaction.atomic():
            for trial in clinicaltrials.clinical_trials():
                trial_obj = self._parse_trial(trial['clinical_study'])
                if trial_obj:
                    count += 1
                    if not count % batch_size:
                        self.stdout.write('{} [{}k]'.format(log_msg, count / 1e3))

        self.stdout.write('%s [OK]' % log_msg)

    def _parse_trial(self, trial):
        lead_sponsor_dict = trial['sponsors']['lead_sponsor']
        lead_sponsor, _ = models.Agency.objects.update_or_create(
            name=lead_sponsor_dict['agency'],
            defaults={
                'type': self._reverse_choices_dict(models.Agency.TYPE_CHOICES)[lead_sponsor_dict['agency_class']]
            }
        )
        self.__validate_object(lead_sponsor)

        trial_obj, _ = models.Trial.objects.update_or_create(
            id=self._id(trial),
            defaults={
                'brief_title': trial['brief_title'],
                'acronym': trial.get('acronym', ''),
                'title': trial.get('official_title', ''),
                'summary': self._clean_string(trial['brief_summary']['textblock'].strip()) if 'brief_summary' in trial else '',
                'description': self._clean_string(trial['detailed_description']['textblock']) if 'detailed_description' in trial else '',
                'lead_sponsor': lead_sponsor,
                'status': self._reverse_choices_dict(models.Trial.STATUS_CHOICES)[trial['overall_status']],
                'start_date': self._parse_date(trial['start_date']) if 'start_date' in trial else None,
                'completion_date': self._parse_date(trial['completion_date']) if 'completion_date' in trial else None,
                'primary_completion_date': self._parse_date(trial['primary_completion_date']) if 'primary_completion_date' in trial else None,
                'phase': self._reverse_choices_dict(models.Trial.PHASE_CHOICES)[trial['phase']],
                'type': self._reverse_choices_dict(models.Trial.TYPE_CHOICES)[trial['study_type']],
                **self._investigator(trial),
                **self._eligibility(trial),
            }
        )
        self.__validate_object(trial_obj)

        for t in self._mesh_terms(trial):
            mesh, _ = models.Mesh.objects.update_or_create(term=t)
            trial_obj.conditions.add(mesh)

        intervention_type_dict = self._reverse_choices_dict(models.Treatment.TYPE_CHOICES)
        for intervention in self._interventions(trial):
            treatment, _ = models.Treatment.objects.update_or_create(
                name=intervention['intervention_name'],
                defaults={
                    'type': intervention_type_dict[intervention['intervention_type']],
                    'name_other': intervention.get('other_name', ''),
                    'description': intervention.get('description', ''),
                }
            )
            self.__validate_object(treatment)
            trial_obj.treatments.add(treatment)

        for collaborator in self._collaborators(trial):
            collaborator_obj, _ = models.Agency.objects.update_or_create(
                name=collaborator['agency'],
                defaults={
                    'type': self._reverse_choices_dict(models.Agency.TYPE_CHOICES)[collaborator['agency_class']]
                }
            )
            self.__validate_object(collaborator_obj)
            trial_obj.collaborators.add(collaborator_obj)

        return trial_obj

    def _id(self, trial):
        nct_id = trial['id_info']['nct_id']
        return int(nct_id[3:])

    def _mesh_terms(self, trial):
        condition_browse = trial.get('condition_browse')
        if not condition_browse:
            return []

        mesh_term = condition_browse['mesh_term']
        if isinstance(mesh_term, str):
            return [mesh_term]
        else:
            return mesh_term

    def _interventions(self, trial):
        interventions = trial.get('intervention', {})
        if not interventions:
            return []

        if isinstance(interventions, dict):
            return [interventions]

        return interventions

    def _collaborators(self, trial):
        collaborators = trial['sponsors'].get('collaborator', [])
        if isinstance(collaborators, list):
            return collaborators
        else:
            return [collaborators]

    def _eligibility(self, trial):
        if 'eligibility' not in trial:
            return {'criteria': '', 'gender': '', 'min_age': '', 'max_age': '', 'healthy_volunteers': None}
        else:
            eligibility = trial['eligibility']
            return {
                'criteria': self._clean_string(eligibility['criteria']['textblock']) if 'criteria' in eligibility else '',
                'gender': self._reverse_choices_dict(models.Trial.GENDER_CHOICES)[eligibility['gender']],
                'min_age': eligibility['minimum_age'].replace('N/A', ''),
                'max_age': eligibility['maximum_age'].replace('N/A', ''),
                'healthy_volunteers': eligibility['maximum_age'] == 'Accepts Healthy Volunteers'
            }

    def _investigator(self, trial):
        if 'responsible_party' in trial:
            return {
                'investigator': trial['responsible_party'].get('investigator_full_name', ''),
                'investigator_institution': trial['responsible_party'].get('investigator_affiliation', ''),
                'investigator_title': trial['responsible_party'].get('investigator_title', ''),
            }
        else:
            return {'investigator': '', 'investigator_institution': '', 'investigator_title': ''}

    def _reverse_choices_dict(self, choices):
        return dict((v, k) for k, v in choices)

    def _clean_string(self, string):
        return re.sub('[ \n\t]+', ' ', string)

    def _parse_date(self, date):
        if not isinstance(date, str):
            date = date['#text']

        try:
            return datetime.datetime.strptime(date, '%B %d, %Y').date()
        except ValueError:
            return datetime.datetime.strptime(date, '%B %Y').date()

    def __validate_object(self, obj):
        if settings.DEBUG:
            obj.full_clean()
