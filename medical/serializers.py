from rest_framework import serializers

from . import models


class MedicalInstitutionListSerializer(serializers.ModelSerializer):
    inst_type = serializers.SerializerMethodField()
    distance = serializers.SerializerMethodField()

    class Meta:
        model = models.MedicalInstitution
        fields = ('id', 'name', 'address', 'postal_code', 'city', 'country', 'url', 'rating', 'latitude', 'longitude', 'inst_type', 'distance')

    def get_inst_type(self, obj):
        return obj.type.name

    def get_distance(self, obj):
        if hasattr(obj, 'distance'):
            return obj.distance
        else:
            return -1


class TreatmentListSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    
    class Meta:
        model = models.Treatment;
        fields = ('name', 'description')

    def get_name(self,obj):
        return obj.name


class MeshListSerializer(serializers.ModelSerializer):
    term = serializers.SerializerMethodField()

    class Meta:
        model = models.Mesh;
        fields = ('term', 'term')

    def get_term(self,obj):
        return obj.term


class MedicalTrialListSerializer(serializers.ModelSerializer):
    summary = serializers.SerializerMethodField()
    treatments = TreatmentListSerializer(many = True, read_only = True)
    conditions = MeshListSerializer(many = True, read_only = True)
    
    class Meta:
        model = models.Trial
        fields = ('brief_title', 'summary', 'type', 'status', 'start_date', 'completion_date', 'investigator', 'treatments', 'conditions')

    def get_summary(self,obj):
        return obj.summary


class MedicalCaregiverListSerializer(serializers.ModelSerializer):
    medical_institutions = MedicalInstitutionListSerializer(many=True, read_only=True)
    profession = serializers.SerializerMethodField()

    class Meta:
        model = models.Caregiver
        fields = ('id', 'name', 'profession', 'medical_institutions')

    def get_profession(self, obj):
        return obj.profession


class MedicalEmploymentListSerializer(serializers.ModelSerializer):
    caregiver_id = serializers.SerializerMethodField()
    caregiver_name = serializers.SerializerMethodField()
    caregiver_profession = serializers.SerializerMethodField()
    institution_id = serializers.SerializerMethodField()

    class Meta:
        model = models.Employment
        fields = ('caregiver_id', 'caregiver_name', 'caregiver_profession', 'institution_id')

    def get_caregiver_id(self, obj):
        return obj.caregiver.id

    def get_caregiver_name(self, obj):
        return obj.caregiver.name

    def get_caregiver_profession(self, obj):
        return obj.caregiver.profession

    def get_institution_id(self, obj):
        return obj.medical_institution.id


class MedicalPublicationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Publication
        fields = "__all__"


class MedicalCaregiverTrialListSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Trial
        fields = ('id', 'brief_title', 'investigator')


class MedicalTrialListSerializer(serializers.ModelSerializer):
    type = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    investigator_caregiver = serializers.SerializerMethodField()
    investigator_institution_relation = serializers.SerializerMethodField()

    distance = serializers.SerializerMethodField()
    latitude = serializers.SerializerMethodField()
    longitude = serializers.SerializerMethodField()

    class Meta:
        model = models.Trial
        fields = ('id', 'brief_title', 'start_date', 'completion_date', 'investigator', 'investigator_caregiver',
                  'investigator_institution', 'investigator_institution_relation', 'status', 'type', 'distance',
                  'latitude', 'longitude')

    def get_type(self, trial):
        return trial.get_type_display()

    def get_status(self, trial):
        return trial.get_status_display()

    def get_investigator_caregiver(self, trial):
        if trial.investigator_caregiver_certainty > 0.7:
            return trial.investigator_caregiver.id

    def get_investigator_institution_relation(self, trial):
        if trial.investigator_institution_relation_certainty > 0.71:
            return trial.investigator_institution_relation.id

    def get_distance(self, trial):
        if hasattr(trial, 'distance'):
            return trial.distance

    def get_latitude(self, trial):
        if hasattr(trial, 'latitude'):
            return trial.latitude

    def get_longitude(self, trial):
        if hasattr(trial, 'longitude'):
            return trial.longitude
