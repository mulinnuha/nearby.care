import math
from functools import lru_cache
from statistics import mean

import re

from django.db.models.functions import Coalesce
from fuzzywuzzy import fuzz

from django.db import models
from django.db.models import Sum, F, Count
from django.utils import timezone
from django.utils.functional import cached_property
from django_countries.fields import CountryField


class LocationManager(models.Manager):
    def nearby(self, latitude, longitude, proximity=1e9):
        """
        Taken from http://stackoverflow.com/a/26219292/1627479
        Return all object which distance to specified coordinates
        is less than proximity given in kilometers
        """
        # Great circle distance formula
        gcd = """
                  6371 * acos(
                   cos(radians(%s)) * cos(radians(latitude))
                   * cos(radians(longitude) - radians(%s)) +
                   sin(radians(%s)) * sin(radians(latitude))
                  )
                  """
        gcd_lt = "{} < %s".format(gcd)
        return self.get_queryset() \
            .exclude(latitude=None) \
            .exclude(longitude=None) \
            .extra(
            select={'distance': gcd},
            select_params=[latitude, longitude, latitude],
            where=[gcd_lt],
            params=[latitude, longitude, latitude, proximity],
            order_by=['distance']
        )


class CaregiverManager(models.Manager):
    def fill_rating(self):
        for caregiver in self.filter(rating__isnull=True):
            caregiver.rating_count = caregiver.employments \
                .all() \
                .aggregate(rating_count=Sum('rating_count'))['rating_count']
            if not caregiver.rating_count:
                caregiver.rating_count = 0
            else:
                caregiver.rating = caregiver.employments \
                    .filter(rating__isnull=False) \
                    .annotate(weighted_rating=F('rating') * (F('rating_count') / caregiver.rating_count)) \
                    .aggregate(rating=Sum('weighted_rating', output_field=models.DecimalField()))['rating']
            caregiver.save()


class AutoDateTimeField(models.DateTimeField):
    def pre_save(self, model_instance, add):
        return timezone.now()


class TimestampModel(models.Model):
    created = models.DateTimeField(default=timezone.now)
    updated = AutoDateTimeField(default=timezone.now)

    class Meta:
        abstract = True


class MedicalInstitutionType(TimestampModel):
    name = models.CharField(max_length=100, db_index=True, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Institution type'
        verbose_name_plural = 'Institution types'
        ordering = ['name']


class MedicalInstitution(TimestampModel):
    objects = LocationManager()

    type = models.ForeignKey(MedicalInstitutionType, related_name='medical_institutions')
   
    
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    city = models.CharField(max_length=255)
    country = CountryField()

    postal_code = models.CharField(max_length=16, blank=True, null=True)
    telephone = models.CharField(max_length=255, blank=True, null=True)
    url = models.URLField(blank=True, null=True)
    rating = models.DecimalField(max_digits=3, decimal_places=1, blank=True, null=True)
    rating_count = models.PositiveIntegerField(default=0)

    # see http://stackoverflow.com/a/26219292/1627479
    latitude = models.FloatField()
    longitude = models.FloatField()


    @cached_property
    def name_clean(self):
        """Remove the '..., locatie ...' part"""
        return self.name.split(', locatie')[0]

    @cached_property
    def location(self):
        if ', locatie ' in self.name:
            return self.name.split(', locatie ')[1]

    def manual_distance(self, latitude, longitude):
        return 6371 * math.acos(
            math.cos(math.radians(latitude)) * math.cos(math.radians(self.latitude))
            * math.cos(math.radians(self.longitude) - math.radians(longitude)) +
            math.sin(math.radians(latitude)) * math.sin(math.radians(self.latitude))
        )

    def __str__(self):
        return '{} ({})'.format(self.name_clean, self.type.name)

    class Meta:
        verbose_name = 'Institution'
        verbose_name_plural = 'Institutions'
        ordering = ['rating', 'rating_count']


class Specialism(TimestampModel):
    medical_institution = models.ForeignKey(MedicalInstitution, related_name='specialisms', blank=True, null=True)

    name = models.CharField(max_length=100, db_index=True, unique=True)
    description = models.TextField()
    waiting_period = models.DurationField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Specialism'
        verbose_name_plural = 'Specialisms'
        ordering = ['name']


class Profession(TimestampModel):
    name = models.CharField(max_length=100, db_index=True, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Profession'
        verbose_name_plural = 'Professions'
        ordering = ['name']


class Caregiver(TimestampModel):
    objects = CaregiverManager()

    medical_institutions = models.ManyToManyField(MedicalInstitution, through='Employment', related_name='caregivers', blank=True)

    name = models.CharField(max_length=100, db_index=True)
    gender = models.NullBooleanField(choices=((True, 'Male'), (False, 'Female')), blank=True, null=True)

    rating = models.DecimalField(max_digits=3, decimal_places=1, blank=True, null=True)
    rating_count = models.PositiveIntegerField(default=0)

    @cached_property
    def profession(self):
        professions = set()
        for employment in self.employments.all():
            professions.add(employment.profession.name)
        return ', '.join(professions)

    @cached_property
    def specialism(self):
        specialisms = set()
        for employment in self.employments.all():
            specialisms.add(employment.specialism.name)
        return ', '.join(specialisms)

    @cached_property
    def institution(self):
        institutions = set()
        for employment in self.employments.all():
            institutions.add(employment.medical_institution.name_clean)
        return '; '.join(institutions)

    @cached_property
    def initials(self):
        # name is in {last_name}, {initial}.{initial}.{...} format
        initials = []
        for initial in self.name.split(', ')[1].split('.'):
            if initial and not initial.startswith(' '):
                initials.append(initial)
        return initials

    @cached_property
    def insertions(self):
        split_name = self.name.split(', ')
        if len(split_name) < 2:
            return

        insertions = split_name[1].split(' ')
        if not insertions:
            return

        return ' '.join(insertions[1:])

    @cached_property
    def surname(self):
        split_name = self.name.split(', ')
        surname = split_name[0]
        insertions = self.insertions
        if insertions:
            return '{} {}'.format(insertions, surname)
        else:
            return surname

    @cached_property
    def publication_count(self):
        if hasattr(self, 'authors'):
            return self.authors.all()\
                .annotate(pub_count=Count('publications'))\
                .aggregate(total_pubs=Coalesce(Sum('pub_count'), 0))['total_pubs']
        else:
            return -1


    @lru_cache()
    def distance_avg(self, latitude, longitude):
        return mean(institution.distance for institution in MedicalInstitution.objects.nearby(latitude, longitude).filter(caregivers=self))

    def __str__(self):
        return '{} ({})'.format(self.name, self.profession)

    class Meta:
        verbose_name = 'Caregiver'
        verbose_name_plural = 'Caregivers'
        ordering = ['name']


class Employment(TimestampModel):
    caregiver = models.ForeignKey(Caregiver, related_name='employments')
    profession = models.ForeignKey(Profession, blank=True, null=True)

    medical_institution = models.ForeignKey(MedicalInstitution, related_name='employments')
    specialism = models.ForeignKey(Specialism, blank=True, null=True)

    rating = models.DecimalField(max_digits=3, decimal_places=1, blank=True, null=True)
    rating_count = models.PositiveIntegerField(default=0)


class Mesh(models.Model):
    term = models.CharField(max_length=255, primary_key=True)

    def __str__(self):
        return self.term

    class Meta:
        verbose_name = 'MeSH term'
        verbose_name_plural = 'MeSH terms'


class Agency(models.Model):
    TYPE_CHOICES = (
        ('O', 'Other'),
        ('N', 'NIH'),
        ('U', 'U.S. Fed'),
        ('I', 'Industry'),
    )

    name = models.CharField(max_length=255, primary_key=True)
    type = models.CharField(max_length=1, choices=TYPE_CHOICES, default='O')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Agency'
        verbose_name_plural = 'Agencies'


class Treatment(models.Model):
    TYPE_CHOICES = (
        ('DS', 'Dietary Supplement'),
        ('RD', 'Radiation'),
        ('BL', 'Biological'),
        ('BH', 'Behavioral'),
        ('PC', 'Procedure'),
        ('OT', 'Other'),
        ('CP', 'Combination Product'),
        ('DR', 'Drug'),
        ('DV', 'Device'),
        ('GN', 'Genetic'),
        ('DT', 'Diagnostic Test')
    )
    
    type = models.CharField(max_length=2, choices=TYPE_CHOICES)
    name = models.CharField(max_length=255, db_index=True)
    name_other = models.TextField(blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Treatment'
        verbose_name_plural = 'Treatments'


class Trial(TimestampModel):
    STATUS_CHOICES = (
        ('US', 'Unknown status'),
        ('RC', 'Recruiting'),
        ('AR', 'Active, not recruiting'),
        ('NR', 'Not yet recruiting'),
        ('CM', 'Completed'),
        ('TA', 'Temporarily not available'),
        ('TN', 'Terminated'),
        ('WH', 'Withheld'),
        ('SP', 'Suspended'),
        ('AV', 'Available'),
        ('WD', 'Withdrawn'),
        ('EI', 'Enrolling by invitation'),
        ('AM', 'Approved for marketing'),
        ('NA', 'No longer available'),
    )
    PHASE_CHOICES = (
        ('NA', 'N/A'),
        ('E1', 'Early Phase 1'),
        ('01', 'Phase 1'),
        ('02', 'Phase 2'),
        ('12', 'Phase 1/Phase 2'),
        ('23', 'Phase 2/Phase 3'),
        ('03', 'Phase 3'),
        ('04', 'Phase 4'),
    )
    TYPE_CHOICES = (
        ('NA', 'N/A'),
        ('EA', 'Expanded Access'),
        ('IN', 'Interventional'),
        ('OP', 'Observational [Patient Registry]'),
        ('OB', 'Observational'),
    )
    GENDER_CHOICES = (
        ('', 'Unknown'),
        ('M', 'Male'),
        ('F', 'Female'),
        ('A', 'All')
    )

    conditions = models.ManyToManyField(Mesh, related_name='trials')
    treatments = models.ManyToManyField(Treatment, related_name='trials')

    brief_title = models.TextField()
    acronym = models.CharField(max_length=100, blank=True)
    title = models.TextField(blank=True)
    summary = models.TextField()
    description = models.TextField(blank=True)

    lead_sponsor = models.ForeignKey(Agency, related_name='trials')
    collaborators = models.ManyToManyField(Agency, related_name='trials_collaborating', blank=True)

    status = models.CharField(max_length=2, choices=STATUS_CHOICES)
    start_date = models.DateField(blank=True, null=True)
    completion_date = models.DateField(blank=True, null=True)
    primary_completion_date = models.DateField(blank=True, null=True)

    phase = models.CharField(max_length=2, choices=PHASE_CHOICES)
    type = models.CharField(max_length=2, choices=TYPE_CHOICES)

    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, blank=True)
    min_age = models.CharField(max_length=10, blank=True)
    max_age = models.CharField(max_length=10, blank=True)
    healthy_volunteers = models.NullBooleanField(blank=True, null=True)
    criteria = models.TextField(blank=True)

    investigator = models.CharField(max_length=100, blank=True)
    investigator_institution = models.CharField(max_length=255, blank=True)
    investigator_title = models.TextField(blank=True)

    investigator_caregiver = models.ForeignKey(Caregiver, related_name='trials', blank=True, null=True)
    investigator_caregiver_uncertainty = models.FloatField(blank=True, default=0)

    investigator_institution_relation = models.ForeignKey(MedicalInstitution, related_name='trials', blank=True, null=True)
    investigator_institution_relation_certainty = models.FloatField(blank=True, default=0)

    @cached_property
    def investigator_initials(self):
        if not self.investigator:
            return []

        first_name = self.investigator.split(' ')[0]
        if '.' in first_name:
            return list(filter(None, first_name.split('.')))
        else:
            return [first_name[0].upper()]

    @property
    def investigator_caregiver_certainty(self):
        """
        The chance of a correct match between investigator and caregiver. In range (0, 1).
        """
        if self.investigator_caregiver_uncertainty == 0:
            return 0
        else:
            # sqrt for making it a bit more uniformly distributed
            return math.sqrt(1 / self.investigator_caregiver_uncertainty)

    def _investigator_caregiver_surname_matches(self):
        """
        Finds the caregivers with the same last name as the trial investigator.
        """
        if not self.investigator:
            return
        last_name = self.investigator.split(' ')[-1]
        matches = Caregiver.objects.filter(name__istartswith=last_name + ',')
        return matches

    def _investigator_caregiver_insertion_matches(self):
        """
        Filters the surname matches on insertions ('van', 'van der', etc).
        """
        surname_matches = self._investigator_caregiver_surname_matches()
        if not surname_matches:
            return None

        insertions1 = ' '.join(self.investigator.split(' ')[:-1]).lower()  # if no first name
        insertions2 = ' '.join(self.investigator.split(' ')[1:-1]).lower()

        matches = []
        for match in surname_matches.all():
            if match.insertions.lower() in (insertions1, insertions2):
                matches.append(match.id)

        return Caregiver.objects.filter(id__in=matches)

    def _investigator_caregiver_initials_matches(self):
        """
        Filter the insertion matches on first initial. Calculates uncertainty metric
        """
        insertion_matches = self._investigator_caregiver_insertion_matches()
        if not insertion_matches:
            return None, 0

        match_initials = [(match.id, match.initials) for match in insertion_matches]

        uncertainty = 0
        match_ids = []
        for id_, initials in match_initials:
            if list(initials) == self.investigator_initials:
                match_ids.append(id_)

        if not match_ids:
            uncertainty += 1
            # No full initial matches, try with first letter(s) only:
            for id_, initials in match_initials:
                if self.investigator and initials[:len(self.investigator_initials)] == self.investigator_initials:
                    match_ids.append(id_)

        if not match_ids and len(self.investigator_initials) == 1:
            uncertainty += 1
            # It could be that the first name inital isn't the first actual initial
            for id_, initials in match_initials:
                if self.investigator and self.investigator_initials[0] in initials:
                    match_ids.append(id_)

        return Caregiver.objects.filter(id__in=match_ids), uncertainty

    def _investigator_caregiver_institution_match(self):
        ratio_threshold = 50
        ratio_certain_enough = 75
        no_institution_match_penalty = 5

        initials_matches, uncertainty = self._investigator_caregiver_initials_matches()
        if not initials_matches:
            return None, 0

        if not self.investigator_institution:
            return initials_matches, uncertainty

        max_ratio = 0
        best_match = None
        for match in initials_matches:
            for institution in match.medical_institutions.all():
                ratio = fuzz.token_sort_ratio(self.investigator_institution, institution.name)
                if ratio > max_ratio:
                    max_ratio = ratio
                    best_match = match

        if max_ratio > ratio_threshold:
            # decrease uncertainty if it's a good match
            uncertainty -= min((max_ratio - ratio_certain_enough) / 10, 1, uncertainty - 1)
            return best_match, uncertainty
        elif max_ratio > 0:
            # return best match but add a penalty
            uncertainty += max(max_ratio / 10,  no_institution_match_penalty)
            return best_match, uncertainty
        else:
            # more penalty if there are alot of matches and we select one 'randomly'
            uncertainty += min(no_institution_match_penalty, len(initials_matches))
            return initials_matches.first(), uncertainty

    def match_investigator_caregiver(self, save=True):
        """
        Match the investigator with a caregiver. Returns the match and certainty. Matches on surname, insertions and
        initials.
        """
        match, uncertainty = self._investigator_caregiver_institution_match()
        if not match:
            return None, 0
        else:
            self.investigator_caregiver = match
            self.investigator_caregiver_uncertainty = uncertainty
            if save:
                self.save()
            return self.investigator_caregiver, self.investigator_caregiver_certainty

    def match_investigator_institution(self, save=True):
        abbrev_regex = re.compile('[A-Z]{2,}')

        if not self.investigator_institution:
            return None, 0

        best_ratio = 0
        best_institution = None
        for institution in MedicalInstitution.objects.all():
            this_name = self.investigator_institution
            other_name = institution.name

            this_abbrev = ''.join(abbrev_regex.findall(this_name))
            other_abbrev = ''.join(abbrev_regex.findall(other_name))

            if not this_abbrev and other_abbrev:
                this_abbrev = self._abbreviate(this_name)
            if this_abbrev and not other_abbrev:
                other_abbrev = self._abbreviate(other_name)

            ratio_abbrev = 0
            if this_abbrev and other_abbrev:
                if this_abbrev in other_abbrev or other_abbrev in this_abbrev:
                    ratio_abbrev = abs(len(this_abbrev) - len(other_abbrev))

            if ratio_abbrev > best_ratio:
                best_ratio = ratio_abbrev
                best_institution = institution

            ratio = fuzz.token_sort_ratio(this_name, other_name)
            if ratio > best_ratio:
                best_ratio = ratio
                best_institution = institution

        if save:
            self.investigator_institution_relation = best_institution
            self.investigator_institution_relation_certainty = best_ratio / 100
            self.save()

        return best_institution, best_ratio

    def _abbreviate(self, str_):
        return ''.join(t[0].upper() for t in str_.split(' ') if t and t[0].isalpha())

    def __str__(self):
        return self.brief_title

    class Meta:
        verbose_name = 'Trial'
        verbose_name_plural = 'Trials'


class Author(TimestampModel):
    name = models.CharField(max_length=255, primary_key=True)
    
    author_caregiver = models.ForeignKey(Caregiver, related_name='authors', blank=True, null=True)
    author_caregiver_uncertainty = models.FloatField(blank=True, default=0)

    @cached_property
    def author_initials(self):
        if not self.name:
            return []

        if ',' in self.name:
            first_name = self.name.split(',')[1].split(' ')[1]
        else:
            first_name = self.name

        if '.' in first_name:
            return list(filter(None, first_name.split('.')))
        else:
            return [first_name[0].upper()]

    @property
    def author_caregiver_certainty(self):
        """
        The chance of a correct match between author and caregiver. In range (0, 1).
        """
        if self.author_caregiver_uncertainty == 0:
            return 0
        else:
            # sqrt for making it a bit more uniformly distributed
            return math.sqrt(1 / self.author_caregiver_uncertainty)

    def _author_caregiver_surname_matches(self):
        """
        Finds the caregivers with the same last name as the author.
        """
        if not self.name:
            return
        last_name = self.name.split(',')[0].split(' ')[-1]
        matches = Caregiver.objects.filter(name__istartswith=last_name + ',')
        return matches

    def _author_caregiver_insertion_matches(self):
        """
        Filters the surname matches on insertions ('van', 'van der', etc).
        """
        surname_matches = self._author_caregiver_surname_matches()
        if not surname_matches:
            return None

        if ',' in self.name:
            first_name = self.name.split(',')[1].split(' ')[1]
            last_name = self.name.split(',')[0]
            name_reordered = first_name + ' ' + last_name
        else:
            name_reordered = self.name

        insertions1 = ' '.join(name_reordered.split(' ')[:-1]).lower()  # if no first name
        insertions2 = ' '.join(name_reordered.split(' ')[1:-1]).lower()

        matches = []
        for match in surname_matches.all():
            if match.insertions is not None:
                if match.insertions.lower() in (insertions1, insertions2):
                    matches.append(match.id)

        return Caregiver.objects.filter(id__in=matches)

    def _author_caregiver_initials_matches(self):
        """
        Filter the insertion matches on first initial. Calculates uncertainty metric
        """
        insertion_matches = self._author_caregiver_insertion_matches()
        if not insertion_matches:
            return None, 0

        match_initials = [(match.id, match.initials) for match in insertion_matches]

        uncertainty = 0
        match_ids = []
        for id_, initials in match_initials:
            if list(initials) == self.author_initials:
                match_ids.append(id_)

        if not match_ids:
            uncertainty += 2
            # No full initial matches, try with first letter(s) only:
            for id_, initials in match_initials:
                if self.name and initials[:len(self.author_initials)] == self.author_initials:
                    match_ids.append(id_)

        if not match_ids and len(self.author_initials) == 1:
            uncertainty += 2
            # It could be that the first name inital isn't the first actual initial
            for id_, initials in match_initials:
                if self.name and self.author_initials[0] in initials:
                    match_ids.append(id_)

        return Caregiver.objects.filter(id__in=match_ids), uncertainty

    def _author_caregiver_match(self):
        ratio_threshold = 50
        ratio_certain_enough = 75

        initials_matches, uncertainty = self._author_caregiver_initials_matches()
        if not initials_matches:
            return None, 0

        max_ratio = 0
        max_ratio_counter = 1
        best_match = None
        for match in initials_matches:
            ratio = fuzz.token_sort_ratio(self.name, match.name)
            if ratio > max_ratio:
                max_ratio = ratio
                best_match = match
            elif ratio == max_ratio:
                max_ratio_counter += 1

        if max_ratio > ratio_threshold:
            if max_ratio_counter == 1:
                # decrease uncertainty if it's a good match
                uncertainty -= min((max_ratio - ratio_certain_enough) / 10, 1, uncertainty - 1)
                return best_match, uncertainty
            else:
                # add penalty if there are alot of matches and we select one 'randomly'
                uncertainty += len(initials_matches)
                return initials_matches.first(), uncertainty
        elif max_ratio > 0:
            # return best match but add a penalty
            uncertainty += max_ratio / 10
            return best_match, uncertainty
        else:
            # more penalty if there are alot of matches and we select one 'randomly'
            uncertainty += len(initials_matches)
            return initials_matches.first(), uncertainty

    def match_author_caregiver(self, save=True):
        """
        Match the author with a caregiver. Returns the match and certainty.
        Matches on surname, insertions and initials.
        """
        match, uncertainty = self._author_caregiver_match()
        if not match:
            return None, 0
        else:
            self.author_caregiver = match
            self.author_caregiver_uncertainty = uncertainty
            if save:
                self.save()
            return self.author_caregiver, self.author_caregiver_certainty

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Author'
        verbose_name_plural = 'Authors'


class Publication(TimestampModel):
    pmid = models.IntegerField(primary_key=True)
    title = models.TextField()
    date = models.DateField(blank=True, null=True)

    authors = models.ManyToManyField(Author, related_name='publications', blank=True)
    mesh_term = models.ManyToManyField(Mesh, related_name='publications', blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Publication'
        verbose_name_plural = 'Publications'
