# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-03-15 16:53
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('medical', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='specialism',
            name='waiting_period',
            field=models.DurationField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='caregiver',
            name='medical_institutions',
            field=models.ManyToManyField(blank=True, related_name='caregivers', through='medical.Employment', to='medical.MedicalInstitution'),
        ),
        migrations.AlterField(
            model_name='employment',
            name='profession',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='medical.Profession'),
        ),
        migrations.AlterField(
            model_name='profession',
            name='name',
            field=models.CharField(db_index=True, max_length=100, unique=True),
        ),
        migrations.AlterField(
            model_name='specialism',
            name='medical_institution',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='specialisms', to='medical.MedicalInstitution'),
        ),
        migrations.AlterField(
            model_name='specialism',
            name='name',
            field=models.CharField(db_index=True, max_length=100, unique=True),
        ),
    ]
