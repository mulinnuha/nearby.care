# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import os
import json


class JsonWriterPipeline(object):

    def open_spider(self, spider):
        self.file = open(os.path.join(os.getcwd(), 'zorgkaartnederland.jl'), 'w', encoding='utf8')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        item_type = type(item).__name__.replace('Item', '')
        item_dict = dict(item)
        item_dict['item_type'] = item_type

        json_line = json.dumps(item_dict, ensure_ascii=False) + '\n'
        self.file.write(json_line)

        return item
