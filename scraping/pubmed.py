from Bio import Entrez
from Bio import Medline
import json
import json_lines
import string
import os
import pprint
import zipfile

from django.conf import settings

DOWNLOAD_DIR = os.path.join(settings.BASE_DIR, 'scraping/data')
MEDICAL_PUBLICATION_FILENAME = 'pubmed.zip'
affiliation_address = "netherlands"
db_name = "pubmed"


def get_pmid_list(address):

    "This get the list of pubmed id based on affiliation address"
    
    keyword = address + ' [AFFL]'
    handle = Entrez.esearch(db = db_name, term = keyword)
    record = Entrez.read(handle)

    id_list = record["IdList"]

    return id_list;

    
def get_metadata_per_pubmed(pubmed_id):
    
    "This get the metadata of a medical publication"
    
    handle = Entrez.efetch(db = db_name, id = pubmed_id, rettype = "medline", retmode = "text")
    record = Medline.read(handle)
    
    pubmed = {}
    pubmed['pmid'] = pubmed_id
    if 'DA' in record:
        pubmed['date'] = record["DA"]
    if 'TI' in record:
        pubmed['title'] = record["TI"]
    if 'AU' in record:
        pubmed['author'] = record["AU"]
    if 'FAU' in record:
        pubmed['author_full'] = record["FAU"]
    if 'MH' in record:
        pubmed['mesh_term'] = record["MH"]
    
    filename = 'pubmed_' + str(pubmed_id) + '.json'
    
    with open(os.path.join(DOWNLOAD_DIR, filename), "w") as outfile:
        json.dump(pubmed, outfile)
    
    return pubmed;


def get_pubmed():

    "This get all pubmed metadata based on affiliation address"
    
    pmid_list = get_pmid_list(affiliation_address)
    total_pubmed = len(pmid_list)
    print('found', total_pubmed, 'medical publication(s)')
    
    counter = 0
    if len(pmid_list) > 0:
        for i in pmid_list:
            counter = counter + 1
            get_metadata_per_pubmed(i)
            print(counter, 'from', total_pubmed, 'files have been retrieved' )

    zip_filename = zipfile.ZipFile(os.path.join(DOWNLOAD_DIR, MEDICAL_PUBLICATION_FILENAME), "w", zipfile.ZIP_DEFLATED)
    for i in pmid_list:
        filename = 'pubmed_' + str(i) + '.json'
        filename_with_path = os.path.join(DOWNLOAD_DIR, filename)
        zip_filename.write(filename_with_path, os.path.relpath(filename_with_path, DOWNLOAD_DIR))
        os.remove(filename_with_path)


def medical_publications():
    pubmed_path = os.path.join(DOWNLOAD_DIR, MEDICAL_PUBLICATION_FILENAME)
    if not os.path.exists(pubmed_path):
        if settings.DEBUG:
            get_pubmed()
        else:
            raise FileNotFoundError('pubmed data not found')

    with zipfile.ZipFile(pubmed_path, 'r') as files:
        for name in files.namelist():
            with files.open(name, 'r') as file:
                jsondata = file.read().decode('utf-8')
                yield json.loads(jsondata)
