import re
import logging

import scrapy
from scrapy.loader import ItemLoader

from .. import items

FIRST_CAP_RE = re.compile('(.)([A-Z][a-z]+)')
ALL_CAP_RE = re.compile('([a-z0-9])([A-Z])')


class ZorgkaartnederlandSpider(scrapy.Spider):

    name = 'zorgkaartnederland'

    @staticmethod
    def _camelcase_to_underscore(name):
        s1 = FIRST_CAP_RE.sub(r'\1_\2', name)
        return ALL_CAP_RE.sub(r'\1_\2', s1).lower()

    def _tab_request(self, response, tab_name, callback=None, meta=None):
        xpath = "//ul[@id='responsive_tabs']//a[text() = '%s']/@href" % tab_name
        tab_url = response.xpath(xpath).extract_first()
        if tab_url:
            next_page = response.urljoin(tab_url)
            return scrapy.Request(next_page, callback=callback, meta=meta)

    def start_requests(self):
        entry_urls = [
            'https://www.zorgkaartnederland.nl/zorginstelling',
            'https://www.zorgkaartnederland.nl/zorgverlener',
        ]
        for entry_url in entry_urls:
            yield scrapy.Request(url=entry_url, callback=self.parse)

    def parse(self, response):
        """
        Parses e.g. https://www.zorgkaartnederland.nl/zorginstelling
        """
        total_pages = int(response.xpath("//ul[@class = 'pagination']//li[last()]//a/text()").extract_first())
        if 'zorginstelling' in response.url:
            next_page_url = 'zorginstelling/pagina%s'
            callback = self.parse_zorginstelling_page
        elif 'zorgverlener' in response.url:
            next_page_url = 'zorgverlener/pagina%s'
            callback = self.parse_zorgverlener_page
        else:
            logging.error('unknown page: ' + response.url)
            return

        for page in range(1, total_pages):
            next_page = response.urljoin(next_page_url % page)
            yield scrapy.Request(next_page, callback=callback)

    def _parse_page(self, response, callback=None):
        next_urls = response.xpath("//ul[@id='results']//li//h4[contains(@class, 'title')]//a/@href").extract()
        for next_url in next_urls:
            next_page = response.urljoin(next_url)
            yield scrapy.Request(next_page, callback=callback)

    def parse_zorginstelling_page(self, response):
        """
        Parses e.g. https://www.zorgkaartnederland.nl/zorginstelling/pagina2227
        """
        yield from self._parse_page(response, callback=self.parse_zorginstelling)

    def parse_zorgverlener_page(self, response):
        """
        Parses e.g. https://www.zorgkaartnederland.nl/zorgverlener/vellema-j-h-b-f-266343
        """
        yield from self._parse_page(response, callback=self.parse_zorgverlener)

    def parse_zorginstelling(self, response):
        """
        Parses e.g. https://www.zorgkaartnederland.nl/zorginstelling/ziekenhuis-groene-hart-ziekenhuis-gouda-110521
        """
        loader = ItemLoader(items.MedicalInstitutionItem(), response=response)

        loader.add_value('id', response.url.split('/')[-1].split('-')[-1])  # last part of the last url dir

        title = response.xpath("//h1[@class = 'title']//span/text()").extract_first()
        if not title:  # not a valid zorginstelling
            return
        loader.add_value('title', title)

        loader.add_xpath('category', "//div[@class = 'organization_title_holder']//p/text()")

        address_props_xpath = "//address//*[@itemprop]"
        address_props = response.xpath("%s/@itemprop" % address_props_xpath).extract()
        address_vals = response.xpath("%s/text()" % address_props_xpath).extract()
        address_dict = dict(zip(address_props, address_vals))
        loader.add_value('address', address_dict.get('streetAddress'))
        loader.add_value('city', address_dict.get('addressLocality'))
        loader.add_value('postal_code', address_dict.get('postalCode'))
        loader.add_value('url', address_dict.get('url'))
        loader.add_value('telephone', address_dict.get('telephone'))

        rating_whole = response.xpath("//div[@itemprop = 'aggregateRating']//span/text()").extract_first()
        rating_decimal = response.xpath("//div[@itemprop = 'aggregateRating']//sup/text()").extract_first()
        if rating_whole and rating_decimal:
            loader.add_value('rating', rating_whole + rating_decimal)
            loader.add_xpath('rate_count', "//*/text()[contains(., 'waarderingen') or contains(., '1 waardering')]")

        item = loader.load_item()

        wachttijden_url = self._tab_request(response, 'Wachttijden', callback=self.parse_wachttijden)
        if wachttijden_url:
            yield wachttijden_url

        kaart_url = self._tab_request(response, 'Service & Contact', callback=self.parse_kaart, meta={'item': item})
        if kaart_url:
            yield kaart_url
        else:
            # item needs to be processed further
            yield item

    def parse_zorgverlener(self, response):
        """
        Parses e.g. https://www.zorgkaartnederland.nl/zorgverlener/cieply-a-h-288332
        """
        loader = ItemLoader(items.CaregiverItem(), response=response)

        id_ = response.url.split('-')[-1]
        loader.add_value('id', id_)

        loader.add_xpath('name', "//h1[@class = 'title']/text()")
        loader.add_xpath('profession', "//div[@class='organization_title_holder']//p//text()")

        address_xpath = "//address//div[@class = 'address_row']"
        address_names = response.xpath('%s//b/text()' % address_xpath).extract()
        address_vals = response.xpath('%s//span/text()' % address_xpath).extract()
        address_dict = dict(zip(address_names, address_vals))

        loader.add_value('gender', address_dict.get('Geslacht'))
        loader.add_value('specialism', address_dict.get('Specialisme'))

        yield loader.load_item()

        # Scrape where the caregiver is employed

        works_at_xpath = "(//div[@class='tab_content_spacer']//div[@class='row'])[3]" \
                         "//ul[contains(@class, 'results_holder')]//li[@class='media']"
        works_at_selectors = response.xpath(works_at_xpath)
        for works_at in works_at_selectors:
            beroep_row = works_at.xpath(".//span[@class='title' and text()='Beroep']/parent::li")
            beroep = beroep_row.xpath(".//span[@class='context']/text()").extract_first()

            zorginstelling_row = works_at.xpath(".//span[@class='title' and text()='Zorginstelling']/parent::li")
            zorginstelling_url = zorginstelling_row.xpath(".//a//@href").extract_first()
            zorginstelling_id = zorginstelling_url.split('-')[-1]

            rating_holder = works_at.xpath(".//div[contains(@class, 'fractional_number_holder')]")
            rating_wrapper = rating_holder.xpath(".//div[contains(@class, 'fractional_number')]")
            rating_whole = rating_wrapper.xpath(".//span/text()").extract_first()
            if rating_whole == '-':
                rating = None
            else:
                rating = rating_whole
                rating_decimal = rating_wrapper.xpath(".//sup/text()").extract_first()
                if rating_decimal:
                    rating += rating_decimal

            rate_count = rating_holder.xpath(".//span[@class='rating_value']/text()").extract_first()

            loader = ItemLoader(items.EmploymentItem(), response=response)
            loader.add_value('medical_institution', zorginstelling_id)
            loader.add_value('caregiver', id_)
            loader.add_value('profession', beroep)
            loader.add_value('rating', rating)
            loader.add_value('rate_count', rate_count)

            yield loader.load_item()

    def parse_wachttijden(self, response):
        """
        Parses e.g. https://www.zorgkaartnederland.nl/zorginstelling/ziekenhuis-maxima-medisch-centrum-locatie-veldhoven-veldhoven-112257/wachttijden
        """
        id_ = response.url.split('/')[-2].split('-')[-1]

        specialisms_xpath = "//ul[contains(@class, 'certificates_table')]//li[@class = 'media']"
        names = response.xpath("%s//span[@title]//@data-original-title" % specialisms_xpath).extract()
        descriptions = response.xpath("%s//span[@title]//@data-original-title" % specialisms_xpath).extract()
        waiting_periods = response.xpath("%s//div[@class='right_media_holder']/text()" % specialisms_xpath).extract()

        for name, description, waiting_period in zip(names, descriptions, waiting_periods):
            loader = ItemLoader(items.SpecialismItem(), response=response)
            loader.add_value('medical_institution', id_)
            loader.add_value('name', name)
            loader.add_value('description', description)
            loader.add_value('waiting_period', waiting_period)
            yield loader.load_item()

    def parse_kaart(self, response):
        """
        Parses e.g. https://www.zorgkaartnederland.nl/zorginstelling/ziekenhuis-maxima-medisch-centrum-locatie-veldhoven-veldhoven-112257/kaart
        """
        # TODO
        item = response.meta['item']
        loader = ItemLoader(item, response=response)

        geo_itemprop_xpath = "//div[@itemprop='geo']//meta[@itemprop='%s']//@content"
        loader.add_xpath('latitude', geo_itemprop_xpath % 'latitude')
        loader.add_xpath('longitude', geo_itemprop_xpath % 'longitude')

        yield loader.load_item()
