# -*- coding: utf-8 -*-

import re
import logging

import scrapy
from scrapy.loader.processors import TakeFirst, MapCompose


def extract_number(text):
    if text:
        try:
            numbers = re.findall(r'\d+', text[0])
            if numbers:
                return int(numbers[0])
            else:
                return 0
        except TypeError:
            logging.error('Error extracting number from: ' + text[0])
    else:
        return 0


def extract_weken_count(text):
    if text:
        if 'week' in text[0]:
            return 1
        elif 'weken' in text[0]:
            return extract_number(text)
        elif extract_number(text):
            logging.warning('Found waiting period with number: ' + text[0])
            return None

    return None


class MedicalInstitutionItem(scrapy.Item):
    id = scrapy.Field(input_processor=MapCompose(int), output_processor=TakeFirst())
    title = scrapy.Field(output_processor=TakeFirst())
    category = scrapy.Field(input_processor=MapCompose(str.strip, str.lower), output_processor=TakeFirst())

    address = scrapy.Field(output_processor=TakeFirst())
    city = scrapy.Field(output_processor=TakeFirst())
    postal_code = scrapy.Field(output_processor=TakeFirst())
    url = scrapy.Field(output_processor=TakeFirst())
    telephone = scrapy.Field(output_processor=TakeFirst())

    rating = scrapy.Field(input_processor=MapCompose(float), output_processor=TakeFirst())
    rate_count = scrapy.Field(input_processor=extract_number, output_processor=TakeFirst())

    latitude = scrapy.Field(input_processor=MapCompose(float), output_processor=TakeFirst())
    longitude = scrapy.Field(input_processor=MapCompose(float), output_processor=TakeFirst())


class EmploymentItem(scrapy.Item):
    medical_institution = scrapy.Field(input_processor=MapCompose(int), output_processor=TakeFirst())
    caregiver = scrapy.Field(input_processor=MapCompose(int), output_processor=TakeFirst())

    profession = scrapy.Field(output_processor=TakeFirst())
    rating = scrapy.Field(input_processor=MapCompose(float), output_processor=TakeFirst())
    rate_count = scrapy.Field(input_processor=extract_number, output_processor=TakeFirst())


class CaregiverItem(scrapy.Item):
    id = scrapy.Field(input_processor=MapCompose(int), output_processor=TakeFirst())

    name = scrapy.Field(output_processor=TakeFirst())
    profession = scrapy.Field(output_processor=TakeFirst())
    gender = scrapy.Field(output_processor=TakeFirst())
    specialism = scrapy.Field(output_processor=TakeFirst())


class SpecialismItem(scrapy.Item):
    medical_institution = scrapy.Field(input_processor=MapCompose(int), output_processor=TakeFirst())

    name = scrapy.Field(output_processor=TakeFirst())
    description = scrapy.Field(output_processor=TakeFirst())
    waiting_period = scrapy.Field(input_processor=extract_weken_count, output_processor=TakeFirst())
