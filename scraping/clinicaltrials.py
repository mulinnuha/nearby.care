import os
import requests
import xmltodict
from zipfile import ZipFile

from django.conf import settings


DOWNLOAD_DIR = os.path.join(settings.BASE_DIR, 'scraping/data')
CLINCALTRIALS_FILENAME = 'clinicaltrials.zip'


def _download_file(url, filename):
    response = requests.get(url, stream=True)
    if response.status_code == 200:
        with open(os.path.join(DOWNLOAD_DIR, filename), 'w+b') as file:
            for chunk in response.iter_content(chunk_size=1024):
                file.write(chunk)


def _download_clinicaltrials_data():
    # Only NL, BE and DE for now
    clinicaltrials_data_url = 'https://clinicaltrials.gov/ct2/results/download?down_stds=all&down_typ=results' \
                              '&down_flds=shown&down_fmt=plain&recr=Open&cntry1=EU%3ANL&show_down=Y'

    print('Downloading clinical trials data...')
    _download_file(clinicaltrials_data_url, CLINCALTRIALS_FILENAME)
    print('Downloaded clinical trials data')


def clinical_trials():
    clinicaltrials_path = os.path.join(DOWNLOAD_DIR, CLINCALTRIALS_FILENAME)
    if not os.path.exists(clinicaltrials_path):
        if settings.DEBUG:
            _download_clinicaltrials_data()
        else:
            raise FileNotFoundError('clinicaltrails data not found')

    with ZipFile(clinicaltrials_path, 'r') as files:
        for name in files.namelist():
            with files.open(name, 'r') as file:
                xml = file.read()
                yield xmltodict.parse(xml)
