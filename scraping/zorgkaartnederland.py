import scrapy
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from scraping.spiders.zorgkaartnederland_spider import ZorgkaartnederlandSpider

process = CrawlerProcess(get_project_settings())
process.crawl(ZorgkaartnederlandSpider)
process.start()
